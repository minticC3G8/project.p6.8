from django.apps import AppConfig


class MissioncoralappConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'missionCoralApp'
