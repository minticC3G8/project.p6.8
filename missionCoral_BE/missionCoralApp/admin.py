from django.contrib import admin
from .models.user import User
from .models.comments import Comments
from .models.corals import Corals

# Register your models here.

admin.site.register(User)
admin.site.register(Corals)
admin.site.register (Comments)