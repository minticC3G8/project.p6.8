from  django.db import models

class Corals(models.Model):
	corals_id= models.BigAutoField(primary_key=True)
	sci_name = models.CharField('Name', null = True, max_length = 100)
	common_name = models.CharField('Common Name', null = True, max_length = 100)
	location = models.CharField('Location', null = True, max_length = 300)
	growth = models.CharField('Growth', null = True, max_length = 200)
	aggressiveness = models.CharField('Aggressiveness', null = True, max_length = 300)
	live_coral = models.ImageField('Live Coral', upload_to = "coralImages/")
	cred_live_coral = models.CharField('Credits Live Coral Photo', null = True, max_length = 200)
	bleach_coral = models.ImageField('Bleach Coral', null = True, upload_to = "coralImages/")
	cred_bleach_coral = models.CharField('Credits Bleached Coral Photo', null = True, max_length = 200)
	dead_coral = models.ImageField('Dead Coral', null = True, upload_to = "coralImages/")
	cred_dead_coral = models.CharField('Credits Dead Coral Photo', null = True, max_length = 200)
	conservation_actions = models.CharField('Conservation actions', max_length = 350)