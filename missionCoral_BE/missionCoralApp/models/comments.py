from django.db import models
from .user import User
from .corals import Corals

class Comments(models.Model):
    id = models.AutoField(primary_key=True)
    user = models.ForeignKey(User, related_name='userComm', on_delete=models.CASCADE)
    coral_id = models.ForeignKey(Corals, related_name='corals', on_delete=models.CASCADE)
    comment = models.CharField('Comment', null=True, max_length =250)

