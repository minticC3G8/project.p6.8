from rest_framework import status, views, generics
from rest_framework.response import Response
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from django.http import Http404
from rest_framework.views import APIView
from ..serializers.commentsSerializer import CommentsSerializer
from ..models.comments import Comments

class CommentsView (APIView):

    serializer_class = CommentsSerializer
    queryset = Comments.objects.all()

    def post(self, request, *args, **kwargs):
        serializer = CommentsSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        return Response(serializer.data, status.HTTP_201_CREATED)

    def get(self, request, id_corals_url):
        lista_comments = Comments.objects.filter(coral_id_id = id_corals_url)
        
        serializer = CommentsSerializer(lista_comments, many = True)
        return Response(serializer.data)

class CommentDetailView(APIView):

    def get_object(self, id):

        try:
            return Comments.objects.get(id=id) 
        except Comments.DoesNotExist:
            raise Http404

    def get(self, request, id_corals_url, id_comment, format =None):
 
        comment = Comments.objects.get(coral_id_id = id_corals_url, id = id_comment)
        serializer = CommentsSerializer(comment)

        return Response(serializer.data)
#'Arrecife/<int:id_corals_url>/comment'
    #'Arrecife/<int:id_corals_url>/comment/<int:id_comment>'

