from rest_framework.views import APIView

from django.http import FileResponse

class CoralImageView(APIView):
    def get(self, request, image_url):
        return FileResponse(open("coralImages/" + image_url, "rb"))
