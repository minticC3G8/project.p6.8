from rest_framework import status, views, generics
from rest_framework.response import Response
from rest_framework_simplejwt.serializers import TokenObtainPairSerializer
from rest_framework.parsers import MultiPartParser, FormParser
from django.http import Http404
from rest_framework.views import APIView
from ..serializers.coralsSerializer import CoralsSerializer
from ..models.corals import Corals

class CoralsView(generics.CreateAPIView):
    parser_classes = [MultiPartParser, FormParser]

    serializer_class = CoralsSerializer
    queryset = Corals.objects.all()

    def post(self, request, *args, **kwargs):
        serializer = CoralsSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        return Response(serializer.data, status.HTTP_201_CREATED)

    def get(self, request):
        lista_corals = Corals.objects.all()
        serializer = CoralsSerializer(lista_corals, many = True)
        return Response(serializer.data)

class CoralsDetailView(APIView):

    def get_object(self, id):

        try:
            return Corals.objects.get(corals_id=id) 
        except Corals.DoesNotExist:
            raise Http404

    def get(self, request, id_corals_url, format=None):
 
        corals = self.get_object(id_corals_url)
        serializer = CoralsSerializer(corals)

        return Response(serializer.data)

    def delete(self, request, id_corals_url, format=None):
        coral = self.get_object(id_corals_url)
        coral.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

    def put(self, request, id_corals_url):
        try:
            coral = Corals.objects.get(corals_id = id_corals_url)
        except Corals.DoesNotExist:
            raise Http404
        
        serializer = CoralsSerializer(coral, data=request.data)
        
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data,
                            status = status.HTTP_202_ACCEPTED)

        return Response(serializer.errors,
                        status = status.HTTP_400_BAD_REQUEST)
        
