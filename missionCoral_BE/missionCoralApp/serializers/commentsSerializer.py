from missionCoralApp.models.comments import Comments
from rest_framework import serializers

class CommentsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Comments
        fields = ['user', 'coral_id', 'comment']