from missionCoralApp.models.corals import Corals
from rest_framework import serializers

class CoralsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Corals
        fields = ['corals_id', 'sci_name', 'common_name', 'location', 'growth', 'aggressiveness', 'live_coral', 'cred_live_coral', 'bleach_coral', 'cred_bleach_coral', 'dead_coral', 'cred_dead_coral', 'conservation_actions']