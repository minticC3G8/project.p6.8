"""missionCoralProject URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from rest_framework_simplejwt.views import TokenObtainPairView,TokenRefreshView
from . import views
from missionCoralApp.views import coralsUpdateView
from missionCoralApp.views import coralsView
from missionCoralApp.views.coralImageView import CoralImageView
from missionCoralApp.views import commentsView
from missionCoralApp.views.userView import UserCreateView
from missionCoralApp.views.userView import UserDetailView


urlpatterns = [
    path('admin/', admin.site.urls),
    path ('', views.mostrarHome),

    # Ruta a galería de corales (CRUD completo)
    path('Arrecife/', coralsView.CoralsView.as_view()),
    path('Arrecife/<int:id_corals_url>', coralsView.CoralsDetailView.as_view()),
    
    #Ruta Comentarios
    path('Arrecife/<int:id_corals_url>/comment', commentsView.CommentsView.as_view()),
    path('Arrecife/<int:id_corals_url>/comment/<int:id_comment>',commentsView.CommentDetailView.as_view()),
    
    #Rutas usuarios 
    path('Haz_parte_del_arrecife/signup/', UserCreateView.as_view()),
    path('Haz_parte_del_arrecife/<int:id_user_url>/',UserDetailView.as_view()),
    path('Haz_parte_del_arrecife/login/', TokenObtainPairView.as_view()),
    path('Haz_parte_del_arrecife/refresh/', TokenRefreshView.as_view()),
    path('Arrecife/images/<str:image_url>', CoralImageView.as_view()),
]
