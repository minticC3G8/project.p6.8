import { createRouter, createWebHistory } from 'vue-router'
import App from './App.vue'
import Home from './Views/Home.vue'
import Login from './Views/Login.vue'
import Signup from './Views/Signup.vue'
import Gallery from './Views/Arrecife.vue'

const routes = [
  {
    path: '/',
    redirect: '/Home',
  
  },

  {
    path: "/home",
    name: "A puerto",
    component: Home
  },

  {
    path:"/Login",
    name: "Haz parte del arrecife-login",
    component: Login

  },

  {
    path:"/Signup",
    name: "Haz parte del arrecife",
    component: Signup
  },

  {
    path:"/Arrecife",
    name: "Arrecife",
    component: Gallery,
  },
    
];

const router = createRouter({
    history: createWebHistory(),
    routes
  })

export default router
